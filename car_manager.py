from turtle import Turtle
from random import randint

COLORS = ["red", "orange", "yellow", "green", "blue", "purple"]
STARTING_MOVE_DISTANCE = 5
MOVE_INCREMENT = 10

class Car(Turtle):
    def __init__(self, shape: str = "square", undobuffersize: int = 1000, visible: bool = True) -> None:
        super().__init__(shape, undobuffersize, visible)
        self.penup()
        self.left(180)
        self.shapesize(1,2)
        self.color(COLORS[randint(0,5)])
        self.goto(300, randint(-260,260))

    def move(self, level = 0):
        self.forward(STARTING_MOVE_DISTANCE + (MOVE_INCREMENT * level))
class CarManager:
    def __init__(self) -> None:
        self.cars : list[Car] = []

    def create_car(self) -> None:
        random = randint(0, 100)

        if random % 5 == 0:
            new_car = Car()
            self.cars.append(new_car)

    def delete_car(self, current_car) -> None:
        current_car.hideturtle()
        self.cars.remove(current_car)

    def check_for_colision(self, player)-> bool:
        for car in self.cars:
            if car.distance(player) < 20:
                return True
        return False
    
    def move_cars(self, level) -> None:
        for car in self.cars:
            car.move(level)
            if car.xcor() <= -300:
                self.delete_car(car)

    def car_count(self) -> int:
        return len(self.cars)

    
    