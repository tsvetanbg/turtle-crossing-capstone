import time
from turtle import Screen
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard

screen = Screen()
screen.setup(width=600, height=600)
screen.tracer(0)
car_manager = CarManager()

player = Player()
scoreboard = Scoreboard()
screen.onkeypress(fun=player.move_up, key="Up")

game_is_on = True
while game_is_on:
    time.sleep(0.1)
    screen.update()
    screen.listen()

    car_manager.create_car()
    car_manager.move_cars(scoreboard.level)

    if car_manager.check_for_colision(player):
        scoreboard.game_over()
        break

    if player.cross_finish():
        scoreboard.level_up()
        player.reset_position()

screen.exitonclick()