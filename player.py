from turtle import Turtle

STARTING_POSITION = (0, -280)
MOVE_DISTANCE = 10
FINISH_LINE_Y = 280


class Player(Turtle):
    def __init__(self, shape: str = "turtle", undobuffersize: int = 1000, visible: bool = True) -> None:
        super().__init__(shape, undobuffersize, visible)
        self.color = "black"
        self.penup()
        self.goto(STARTING_POSITION)
        self.left(90)
    
    def move_up(self):
        self.forward(MOVE_DISTANCE)
    
    def cross_finish(self) -> bool:
        return True if self.ycor() >= FINISH_LINE_Y else False
    
    def reset_position(self):
        self.goto(STARTING_POSITION)