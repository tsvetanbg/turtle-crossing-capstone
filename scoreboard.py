from turtle import Turtle

FONT = ("Courier", 24, "normal")

class Scoreboard(Turtle):
    def __init__(self, shape: str = "classic", undobuffersize: int = 1000, visible: bool = False) -> None:
        super().__init__(shape, undobuffersize, visible)
        self.level = 1
        self.penup()
        self.goto(-280, 260)
        self.write(f"Level: {self.level}",font=FONT)

    def level_up(self):
        self.level += 1
        self.clear()
        self.write(f"Level: {self.level}",font=FONT)

    def game_over(self):
        self.goto(0,0)
        self.write("GAME OVER!", font=FONT, align="center")
        self.shapesize(2,2)